# What is it ?

Demo FastAPI server with A/B testing

Split traffic based on user session (not each request)

Try it with `test_main.py` or curl

# TODO 

First user is always beta tester option

# See also

https://stackoverflow.com/questions/61228347/split-traffic-in-rest-api-for-a-b-testing

https://stackoverflow.com/questions/68590502/script-that-splits-traffic

https://stackoverflow.com/questions/72482499/how-to-split-traffic-from-same-url-path-to-2-different-upstreams (Load-Balancing via nginx upstream weight)

https://www.nginx.com/blog/performing-a-b-testing-nginx-plus/ (nginx split_clients)

https://developers.cloudflare.com/workers/examples/ab-testing/ (cookie analysing)


