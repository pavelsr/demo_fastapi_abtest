import math
import random
import tempfile
from fastapi.testclient import TestClient
from main import app

def calc_requests_qty(percentage):
    return int(100/percentage) if 100 % percentage == 0 else int(100/math.gcd(100, percentage))

for x in [ 10, 20, 25, 50 ]:
    app.ab_percentage = x
    request_num = 1
    start_uid = 1
    amount = calc_requests_qty(app.ab_percentage)

    open(app.BETA_TESTERS_FILENAME, "w").close()
    app.sessions = []

    # app.BETA_TESTERS_FILENAME = tempfile.NamedTemporaryFile(delete_on_close=False).name  # works only in python=3.12
    # print("Temp file : ", app.ab_fh.name)
    print("### Beta percentage : ",app.ab_percentage)
    print("Total minimal requests for fully test : ",amount) # amount

    client = TestClient(app)
    response = client.get("/")
    assert response.status_code == 422  # no user_id provided

    for user_id in range(start_uid,amount):
        response = client.get("/", params={ "user_id":user_id } )
        assert response.status_code == 200
        result = response.json()
        print("Request #", request_num, "for#1", "user_id =", user_id, result["message"])
        assert result == app.DEFAULT_MSG
        request_num += 1

    for _ in range(random.randint(2, 5)): # same user_id 2 times
        user_id = amount
        response = client.get("/", params={ "user_id":user_id } )
        assert response.status_code == 200
        result = response.json()
        print("Request #", request_num, "user_id =", user_id, result["message"])
        assert result == app.BETA_MSG
        request_num += 1

    user_id = random.randint(start_uid, amount-1)   # any user_id from first cycle
    response = client.get("/", params={ "user_id":user_id } )
    assert response.status_code == 200
    result = response.json()
    print("Request #", request_num, "user_id =", user_id, result["message"])
    assert result == app.DEFAULT_MSG, "New request from user with active session doesn't change A/B state"
    print("###############")
