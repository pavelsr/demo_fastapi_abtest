##############################################
# Demo app that shows how to do A/B test     #
# By default, every 10th user is beta tester #
# (you can change it via AB_PERCENT env var) #
# test it with `uvicorn main:app --reload`   #
##############################################
import os
import logging
from fastapi import FastAPI

app = FastAPI()
app.sessions = []
app.ab_percentage = os.getenv("AB_PERCENT", 20)
app.BETA_MSG = { "message": "You are beta tester" }
app.DEFAULT_MSG = { "message": "You are regular user" }
app.BETA_TESTERS_FILENAME=os.getenv("BETA_UIDS_FILENAME", 'beta_testers.txt')
logger = logging.getLogger('uvicorn.error')

def get_beta_testers():
    with open(app.BETA_TESTERS_FILENAME, "r") as file:
        return [line.rstrip() for line in file]

def is_beta_tester(user_id):
    app.beta_user_ids = get_beta_testers()
    if app.beta_user_ids:
        logger.info("Beta users : %s", ", ".join(app.beta_user_ids) )
    else:
        logger.info("No active beta users")
    if user_id in app.beta_user_ids:
        return True
    else:
        return False

def add_beta_tester(user_id):
    #print("Writing new beta tester")
    with open(app.BETA_TESTERS_FILENAME, "a+") as file:
        file.write(user_id + "\n")

@app.get("/")
async def root(user_id):
    logger.info("GET /")
    is_new_nth_user = None

    if user_id not in app.sessions:
        app.sessions.append(user_id)
        is_new_nth_user = len(app.sessions) % round(100/app.ab_percentage)

    logger.info("Active users count : %s", len(app.sessions))
    is_beta = 0

    if is_beta_tester(user_id):
        is_beta = 1
        msg = "Cond #1 : user_id={} is already in beta testers"
    elif is_new_nth_user == 0:
        is_beta = 1
        msg = "Cond #2 : user_id={} will be beta tester"
        add_beta_tester(user_id)
    else:
        msg = "Cond #3 : user_id={} will be regular user"

    msg = msg.format(user_id)
    logger.info(msg)
    #print("->", "nth=",is_new_nth_user, " ", msg)

    return app.BETA_MSG if is_beta else app.DEFAULT_MSG

if __name__ == "__main__":
    import uvicorn
    uvicorn.run(app, host="0.0.0.0", port=8000)
